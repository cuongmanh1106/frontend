import { namespaceHTML } from "@angular/core/src/render3/instructions";

export class Student {
    id:number;
    name:string;
    khoa:string;
    status:boolean;

    constructor(name:string, khoa:string, status:boolean) {
        this.name = name;
        this.khoa = khoa;
        this.status = status;
    }

}