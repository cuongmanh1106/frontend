import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {Student } from './../models/student.class';
@Injectable()
export class studentService {
    url:string = 'http://5b7f8267af5e5600144d5ec4.mockapi.io/api/Sudents';

  constructor(private Http:Http) { }

  getData():Observable<Student[]>{
    return this.Http.get(this.url).map((response: Response)=>response.json());
  }

  delete(id:number) {
      return this.Http.delete(this.url+"/"+id).map(res=>res.json);
  }

  addData(student:Student) {
    return this.Http.post(this.url,student).map(res=>res.json());
  }

  editData(id:number,student:Student) {
      return this.Http.put(this.url+"/"+id,student).map(res=>res.json());
  }

  handleError(err) {
    if(err.error instanceof Error) {
        console.log('Client error:'+ err.error.message); //lỗi do người lập trình nếu lỗi truyền vào có kiểu là error
    } else {
        console.log('Server error: ${err.error.status} - ${err.error}'); //lỗi server
    }
}

}