import { Component, OnInit } from '@angular/core';
import { Student } from '../models/student.class';
import {studentService } from './student.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  constructor(private studentService:studentService) { }

  arr:Student[] = [];

  student:Student = new Student('','',true);

  ngOnInit() {
    this.loadData();
  }
  add() {
    this.student = new Student('','',true);
  }

  addData() {
    this.studentService.addData(this.student).subscribe(data =>{
      this.loadData();
    },error=>{
      this.studentService.addData(error);
    })

   
  }
  edit(item) {
    this.student = item;
  }

  editData() {
    this.studentService.editData(this.student.id,this.student).subscribe(data=>{
      this.loadData();
    },error=>{
      this.studentService.handleError(error);
    })
  }

  loadData(){
    this.studentService.getData().subscribe(data=>{
      this.arr = data;
      console.log(data);
    },error=>{
      this.studentService.handleError(error);
    })
  }

  deleteData(id:number) {
    if(confirm('are you sure')){
      this.studentService.delete(id).subscribe(data=>{
        this.loadData();
      },error=>{
        this.studentService.handleError(error);
      })
    }
    }
   

}
